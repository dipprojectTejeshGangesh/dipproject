function myCED( alpha, sigma, tMax, rho )
%% Input of images
im = imread('../images/2.png');
[numrow ,numcol, d] = size(im);

%% Initialization
t = 0;
stepT = 2;
wsigma = 50;
wrho = 50;
usigma = double(im);
C = 1;
while (t < tMax)
    t = t+stepT;
    %% gaussian
    limitx = -ceil(wsigma*sigma):ceil(wsigma*sigma);
    Ksigma = exp(-(limitx.^2/(2*sigma^2)));
    Ksigma = Ksigma/sum(Ksigma(:));
    usigma=imfilter(imfilter(usigma, (Ksigma'), 'same' ,'replicate'), Ksigma, 'same' ,'replicate');
    
    %% Gradient
    sobelx = [3 10 3; 0 0 0; -3 -10 -3]/32;
    sobely = [3 0 -3; 10 0 -10; 3 0 -3]/32;
    ux=imfilter(usigma,sobelx,'conv','same','replicate');
    uy=imfilter(usigma,sobely,'conv','same','replicate');
    
    %% Structured tensors
    limitxJ = -ceil(wrho*rho):ceil(wrho*rho);
    KsigmaJ = exp(-(limitxJ.^2/(2*rho^2)));
    KsigmaJ = KsigmaJ/sum(KsigmaJ(:));
    Jxx=imfilter(imfilter((ux.^2), (KsigmaJ'), 'same' ,'replicate'), KsigmaJ, 'same' ,'replicate');
    Jxy=imfilter(imfilter((ux.*uy), (KsigmaJ'), 'same' ,'replicate'), KsigmaJ, 'same' ,'replicate');
    Jyy=imfilter(imfilter((uy.^2), (KsigmaJ'), 'same' ,'replicate'), KsigmaJ, 'same' ,'replicate');
    
    %% Eigen vectors
    v1x = zeros(numrow, numcol);
    v1y = zeros(numrow, numcol);
    v2x = zeros(numrow, numcol);
    v2y = zeros(numrow, numcol);
    lambda1 = zeros(numrow, numcol);
    lambda2 = zeros(numrow, numcol);
    coherenceK = zeros(numrow, numcol);
    for i=1:numrow
        for j=1:numcol
            pixelTensor = [Jxx(i,j), Jxy(i,j); Jxy(i,j), Jyy(i,j)];
            [pixelV,pixelD] = eig(pixelTensor);
            v1x(i,j) = pixelV(1,1);
            v2x(i,j) = pixelV(1,2);
            v1y(i,j) = pixelV(2,1);
            v2y(i,j) = pixelV(2,2);
            lambda1(i,j) = pixelD(1,1);
            lambda2(i,j) = pixelD(2,2);
            coherenceK(i,j) = (lambda1(i,j) - lambda2(i,j))^2;
            if(coherenceK(i,j) == 0)
                lambda1(i,j) = alpha;
            else
                lambda1(i,j) = alpha + (1 - alpha)*exp(-C/coherenceK(i,j));
            end;
            lambda2(i,j) = alpha;
        end;
    end;
    Dxx = lambda1.*v1x.^2   + lambda2.*v2x.^2;
    Dxy = lambda1.*v1x.*v1y + lambda2.*v2x.*v2y;
    Dyy = lambda1.*v1y.^2   + lambda2.*v2y.^2;
    
    imout=diffusion_scheme_2D_implicit(usigma,Dxx,Dxy,Dyy, stepT);
end;

%% Output of images
figure(1);
subplot(1, 2, 1);
imagesc(im);
title('Original image');
colormap('Gray');
daspect ([1 1 1]);

subplot(1, 2, 2);
imagesc(imout);
title('Coherence Enhancing Diffusion Filtering');
colormap('Gray');
daspect ([1 1 1]);

figure(2);

subplot(1, 2, 1);
imagesc(im);
title('Original image');
colormap('Gray');
daspect ([1 1 1]);

subplot(1, 2, 2);
imagesc(atan2(double(uy), double(ux)));
title('Orientation of smooth gradient');
colormap('Gray');
daspect ([1 1 1]);

imwrite((uint8(im)), '../images/2CED.png');
end